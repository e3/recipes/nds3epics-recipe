# nds3epics conda recipe

Home: "https://gitlab.esss.lu.se/epics-modules/nds3epics"

Package license: EPICS Open License

Recipe license: BSD 3-Clause

Summary: EPICS implementation of NDS3
